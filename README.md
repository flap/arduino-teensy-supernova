Improved POI implementation
===========================

This is a cleaned-up variant of the original project for a friend of mine. It
tries to be more user friendly and error resistant. What it is intended for:

- a µC of the Teensy-3.x family or newer (Teensy-4.x recommended)
- a connected LED stripe of type DotStar (tested with up to 432 LEDs)
- image files to display
- IR receiver
- Adafruit "Mini Remote Control"

Use the Python script
---------------------

Note: the script to convert the images can now be used with Python-3.x.

    ./convert.py <list of image files>

The script converts the image file content into plain C code and outputs it to
*stdout*. Redirect it into the the "performance/graphics.h" file to include it
into the binary later on.

The images have some restrictions: refer information in file "performance.ino"
for details.

Define your performance
-----------------------

Edit the file "performance/performance.ino" and follow the comments to populate
the "perf[]" table.

Also read the comments about the 'ENABLE_IRREMOTE' macro and the variables
'available_led_count', 'pattern_repeat_count', 'line_interval_min_us',
'line_interval_us', 'line_interval_max_us' and 'led_brightness' in the same
file.

IR remote control
-----------------

When IR remote control is enabled (refer 'ENABLE_IRREMOTE' macro in
"performance/performance.ino") the performance doesn't start automatically. It
waits for the "START/PAUSE" button pressed event instead.

When the performance is running, a "START/PAUSE" button press pauses the
performance. "Pauses" means the current image is repeated until a second press
of the "START/PAUSE" button. The performance then continues where it pauses.

The "STOP/MODE" button stops the currently running/pausing performance. You can
restart the performance from the beginning with the "START/PAUSE" button in this
case.

The "REPEAT" button re-starts the performance from the beginning (without the
way through the "STOP/MODE" button followed by the "START/PAUSE" button).

The "VOL-" button slows down the image presentation, e.g. each image line is
shown longer than the default (set via variable line_interval_us in
"performance/performance.ino").

The "VOL+" button speeds up the image presentation, e.g. each image line is
shown shorter than the default (set via variable line_interval_us in
"performance/performance.ino").

Note: for the line intervall you can set min/max values via variables
line_interval_min_us/line_interval_max_us in "performance/performance.ino" to
avoid values which makes no sense.

The "UP" button increases the LED's brightness. If the default is already full
brightness (set via variable led_brightness in "performance/performance.ino"),
nothing happens.

The "DOWN" button decreases the LED's brightness until all LEDs are off.

All other buttons are ignored.

Background
----------

Everything in the Arduino-IDE is C++, but I used plain C for this implementation.
It took me a while to understand how the Arduino-IDE deals with the sources, but
now it should compile successful without annoying error messages.

Original project text
---------------------

## This repository has been archived

The contents were mirrored to https://github.com/adafruit/Adafruit_Learning_System_Guides/tree/master/Kinetic_POV

## Kinetic_POV

Software related to DotStar LED persistence-of-vision performance items -- initially just the Trinket-based "Genesis Poi," but might devise more POV projects later...this is where they'll go.

Guide: https://learn.adafruit.com/genesis-poi-dotstar-led-persistence-of-vision-poi

Requires DotStar library for Arduino: https://github.com/adafruit/Adafruit_DotStar

Adafruit invests time and resources providing this open source code, please support Adafruit and open-source hardware by purchasing products from [Adafruit.com](https://www.adafruit.com).

Written by Phil Burgess / Paint Your Dragon for Adafruit Industries. MIT license, all text above must be included in any redistribution. See 'COPYING' file for additional notes.
