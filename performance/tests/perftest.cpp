#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <sys/time.h>

static struct timespec start_time;

// kept the order of operands the same, that is a - b = result
# define timespec_diff_macro(a, b, result)            \
  do {                                                \
    (result)->tv_sec = (a)->tv_sec - (b)->tv_sec;     \
    (result)->tv_nsec = (a)->tv_nsec - (b)->tv_nsec;  \
    if ((result)->tv_nsec < 0) {                      \
      --(result)->tv_sec;                             \
      (result)->tv_nsec += 1000000000;                \
    }                                                 \
  } while (0)

static void delay(__attribute__((unused)) unsigned ms)
{
	return;
}

static unsigned long millis(void)
{
	struct timespec ct, offset;
	unsigned long time_flows_by;

	clock_gettime(CLOCK_MONOTONIC_RAW, &ct);
	timespec_diff_macro(&ct, &start_time, &offset);

	time_flows_by = offset.tv_sec * 1000 + offset.tv_nsec / 1000000;
	return time_flows_by * 50; /* my desktop CPU has much more power */
}

static unsigned long micros(void)
{
	struct timespec ct, offset;
	unsigned long time_flows_by;

	clock_gettime(CLOCK_MONOTONIC_RAW, &ct);
	timespec_diff_macro(&ct, &start_time, &offset);

	time_flows_by = offset.tv_sec * 1000000 + offset.tv_nsec / 1000;
	return time_flows_by * 50; /* my desktop CPU has much more power */
}

#define TEST
#include "../performance.ino"
#include "../implementation.ino"

int main(void)
{
	clock_gettime(CLOCK_MONOTONIC_RAW, &start_time);

	setup();

	while (performance_idx >= 0) {
		loop();
	}

	return 0;
}
