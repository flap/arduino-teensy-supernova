Simple test to check if it works
================================

Run the implementation on the host to check if it works as intended. Debugging
on the real hardware is much harder, so begin on the development host instead.

This is a minimalistic attempt to check if the statemachines for the image lines
and performance progression work as intended.
