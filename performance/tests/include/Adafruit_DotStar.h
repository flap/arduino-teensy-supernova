/* some fake declarations */
#include <stdint.h>

#define DOTSTAR_BRG (1 | (2 << 2) | (0 << 4))
#define DOTSTAR_BGR (2 | (1 << 2) | (0 << 4))

class Adafruit_DotStar {
public:
  Adafruit_DotStar(__attribute__((unused)) uint16_t n, __attribute__((unused)) uint8_t o = DOTSTAR_BRG) { ; }
  Adafruit_DotStar(__attribute__((unused)) uint16_t n, __attribute__((unused)) uint8_t d, __attribute__((unused)) uint8_t c, __attribute__((unused)) uint8_t o = DOTSTAR_BRG) { ; }
  ~Adafruit_DotStar(void) { ; }

  void begin(void) { ; }
  void show(void) { ; }
  void setPixelColor(__attribute__((unused)) uint16_t n, __attribute__((unused)) uint8_t r, __attribute__((unused)) uint8_t g, __attribute__((unused)) uint8_t b) { ; }
  void fill(__attribute__((unused)) uint32_t c = 0, __attribute__((unused)) uint16_t first = 0, __attribute__((unused)) uint16_t count = 0)  { ; }
  void clear() { ; }
};
