/* SPDX-License-Identifier: CC0-1.0 */

#include <stdint.h>
#include "implementation.h"

/* ----- don't touch anything above this line ------------ */

/**
 * Define this macro (e.g. uncomment it) if you want Infrared Remote Control.
 *
 * @attention If you enable this macro, the performance no longer starts
 *            automatically. It waits for the the PLAY/Pause button instead.
 *            If you want a different behaviour, refer #ENABLE_PERF_ON_START
 */
#define ENABLE_IRREMOTE

/**
 * Enable this macro if you want to start the performance automatically, even
 * if #ENABLE_IRREMOTE is enabled. In this case the PLAY/PAUSE button isn't
 * required to start the performance after switching on the device.
 */
#define BEGIN_PERF_ON_START

/**
 * Define here the count of really attached LEDs. Usually this is the same
 * value than the NUM_LEDS macro has (defined by the convert.py script).
 *
 * Regular use is: the image's height matches the count of available LEDs.
 * Take into account:
 *  - if you are use images with two colours: the image's height and
 *    the available LED count must be a multiple of eight
 *  - if you are use images with 16 colours: the image's height and
 *    the available LED count must be a multiple of two
 *
 * There might be more LEDs available than your image's height. In this case
 * change the @b pattern_repeat_count below.
 *
 * To calculate the amount of time to send the data:
 * - the Teensy 3.2 processor at 75 MHz uses a 12 MHz clock at the SPI signals
 * - between each transfered byte as small delay can be seen
 * - so, it takes about
 *   - 200 µs to send the data for 40 LEDs
 *   - 2,16 ms to send the data for 432 LEDs
 * - the Teensy 4.x processor at 750 MHz uses a 12 MHz clock at the SPI signals
 * - between each transfered byte as very small delay can be seen
 * - so, it takes about
 *   - 1,26 ms to send the data for 432 LEDs
 *
 * To calculate the amount of time to calculate the data:
 * - for the Teensy 3.2 processor at 75 MHz
 *   - 32 µs to calculate the data for a 40 LEDs row
 *   - 800 µs to calculate the data for a 432 LEDs row
 * - for the Teensy 4.x processor at 750 MHz
 *   - 24 µs to calculate the data for a 432 LEDs row
 */
static const unsigned available_led_count = 432;

/**
 * If you have more LEDs available than your image's height, you can repeat the
 * image's line pattern to fill up all available LEDs.
 * Usually if @b available_led_count is equal to @b NUM_LEDS, @b pattern_repeat_count
 * is '1'.
 *
 * Keep in mind: the image's height and the repeat count must match exactly
 * the count of available LEDs. Example: if your image's height is 128 and your
 * available LED count is 384, you can repeat your image pattern three times.
 * Pay attention to the notes for @b available_led_count if your image's height
 * is odd.
 */
static const unsigned pattern_repeat_count = 3;

/**
 * Time in [µs] a single image line should be visible
 * 750 lines per second -> ~1.3 ms per line
 *
 * @attention Estimate the sum of the time to send the data to the LEDs and
 *            calculate the time to calculate the next line.
 *
 * @note Define in the @b line_interval_max_us the slowest value that
 *       should be set via IR remote control "VOL-"
 *
 * @note Define in the @b line_interval_min_us the fastest value that should be
 *       set via IR remote control "VOL+"
 *
 * Example:
 *  - time to send data to 40 LEDs: 200 µs
 *  - time to calculate the next line with 40 LEDs: 40 µs
 *    - processing one line needs about 250 µs, e.g. 4000 lines per second
 *
 *  - time to send data to 432 LEDs: 2.16 ms
 *  - time to calculate the next line with 432 LEDs: 800 µs
 *    - processing one line needs about 3 ms, e.g. 300 lines per second
 *
 * With IR remote control enabled (refer #ENABLE_IRREMOTE), you can adapt the
 * line interval at run-time. In this case you should define a min/max value in
 * which the interval can be changed to avoid invalid and non working values.
 *
 * IR remote example:
 *
 *  (1000000UL / 800UL)      (1000000UL / 750UL)      (1000000UL / 300UL)
 *        1250 µs                 1333 µs                   3333 µs
 * --------|<-----------------------|------------------------->|----------
 *         ^                        ^                          ^
 * line_interval_min_us      line_interval_us          line_interval_max_us
 *
 * The interval starts at 1333 µs and can be decreased down to 1250 µs with the
 * VOL+ button and increased up to 3333 µs with the VOL- button.
 */
static unsigned long line_interval_us = 1000000UL / 750UL;
#ifdef ENABLE_IRREMOTE
static const unsigned long line_interval_min_us = 1000000UL / 800UL;
static const unsigned long line_interval_max_us = 1000000UL / 300UL;
#endif

/**
 * Setup initial LED brightness
 *
 * According to the DotStar library a value of '255' meas full brightness, e.g.
 * no change on the input data.
 *
 * '255' means full brightness
 * '0' means more or less all LEDs are off
 */
static uint8_t led_brightness = 255;

/**
 * Define the performance
 *
 * Each line in this performance array is meant as:
 *
 *  { "Point of time", "what should happen", },
 *
 * Define the "point of time"
 * --------------------------
 *
 * Use "AT(some time)" to define something happens at this point of time after the
 * start of the performance. "some time" can be a float and is meant in seconds.
 * The resolution of this point of time is milliseconds. E.g. a value of "2.56"
 * for "some time" means 2 seconds and 560 milliseconds
 *
 * Keep in mind: it depends on the computing power of the used CPU how exactly
 * the program can hit this time. So don't expect it always hits the millisecond.
 * The expectation should be: if the current time is equal or later to this
 * defined point of time, then the "what should happen" will happen.
 *
 * Define "what should happen"
 * ---------------------------
 *
 * Use the IMAGE_NO(some number) to switch to this image at this point of time.
 *  - note: "some number" starts with "0" for the first image, "1" for the second...
 *
 * Use PERF_REPEAT to restart the performance at this point of time.
 *
 * Use PERF_STOP to stop the performance at this point of time.
 *
 * Notes
 * -----
 *
 * - the performance array *must* contain at least two entries (one image
 *   activation and one repeat or stop entry)
 * - there is no limit of the line count in the performance array. Only the
 *   available flash memory limits it (and everything else, too)
 * - the time always starts at "0" when the performance starts and then just
 *   increases.
 * - the "what should happen" "PERF_REPEAT" resets the time to "0" again and
 *   re-starts at the first line in the performance array
 * - ensure the "point of time" in each line increases, never define a time
 *   smaller or equal than its predecessor:
 *     { AT(3.5), IMAGE_NO(1), },
 *     { AT(4.1), IMAGE_NO(2), }, \<--- okay
 *     { AT(3.6), IMAGE_NO(3), }, \<--- time goes backwards! Not allowed!!
 * - if all images have the same line count, the transition from one image to the
 *   next is smooth, e.g. the new image continues at the same line the last image
 *   stops. If the line count of both images differs, the new image starts at its
 *   line 0 (might result in visible artefacts).
 *
 * Examples
 * --------
 *
 * { AT(0), IMAGE_NO(0), },
 *
 * Show image no "0" right at the beginning of the performance.
 *
 * { AT(4.1), IMAGE_NO(2), },
 *
 * Show image no "2" when the time has reached 4 seconds and 100 milliseconds
 *
 * { AT(8.8), PERF_REPEAT, },
 *
 * Repeats the whole performance when the time has reached 8 seconds and 800 milliseconds
 *
 * { AT(12.5), PERF_STOP, },
 *
 * Stops the performance when the time has reached 12 seconds and 500 milliseconds.
 * This turns off the LEDs to save power.
 *
 * If you made a mistake here:
 *
 * At run-time the LEDs will show some detectable errors:
 *  - if you setup an erroneous time
 *    - in this case a specific number of red LEDs will blink to show the bad entry
 *      in the performance table. One red LED means the error is in the first line,
 *      two red LEDs the error is in the second line and so forth.
 *  - the performance array must contain at least two entries
 *    - if the performance array is too short, yellow LEDs will blink
 */
static const struct performance perf[] = {
      {  AT(0.0), IMAGE_NO(0), },  // anfangen mit schwarz
      {  AT(4.0), IMAGE_NO(1), },
      {  AT(8.0), IMAGE_NO(2), },
      {  AT(12.0), IMAGE_NO(3), },
      {  AT(16.0), IMAGE_NO(4), },
      {  AT(20.0), IMAGE_NO(5), },
      {  AT(24.0), IMAGE_NO(6), },
      {  AT(28.0), IMAGE_NO(7), },
      {  AT(32.0), IMAGE_NO(8), },
      {  AT(36.0), IMAGE_NO(9), },
      {  AT(40.0), IMAGE_NO(10), },
      {  AT(44.0), IMAGE_NO(11), },
      {  AT(48.0), IMAGE_NO(12), },
      {  AT(52.0), IMAGE_NO(13), },
      {  AT(56.0), IMAGE_NO(14), },
      {  AT(60.0), IMAGE_NO(15), },
      {  AT(64.0), IMAGE_NO(16), },
      {  AT(68.0), IMAGE_NO(0), }, // enden mit schwarz
      {  AT(72.0), PERF_REPEAT, },
};

/* ----- don't touch anything below this line ------------ */

static const size_t performance_length = (sizeof(perf) / sizeof(perf[0]));

// kate: tab-indents on; tab-width 4; indent-width 4; replace-tabs off; eol unix; hl = c++; keep-extra-spaces false; remove-trailing-spaces all; show-tabs on;
