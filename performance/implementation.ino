/* SPDX-License-Identifier: CC0-1.0 */
/* SPDX-License-Identifier: MIT */
/* SPDX-FileCopyrightText: 2015 Adafruit Industries */

/*------------------------------------------------------------------------
  POV IR Supernova Poi sketch.  Uses the following Adafruit parts
  (X2 for two poi):

  - Teensy 3.2
  - 2200 mAh Lithium Ion Battery https://www.adafruit.com/product/1781
  - LiPoly Backpack https://www.adafruit.com/product/2124
  - 144 LED/m DotStar strip (#2328 or #2329)
    (ONE METER is enough for TWO poi)
  - Infrared Sensor: https://www.adafruit.com/product/157
  - Mini Remote Control: https://www.adafruit.com/product/389
    (only one remote is required for multiple poi)

  Needs Adafruit_DotStar library: github.com/adafruit/Adafruit_DotStar

  This is based on the LED poi code (also included in the repository),
  but ATtiny-specific code has been stripped out for brevity, since the
  staffs pretty much require Pro Trinket or better (lots more LEDs here).

  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!

  Written by Phil Burgess / Paint Your Dragon for Adafruit Industries.
  MIT license, all text above must be included in any redistribution.
  See 'COPYING' file for additional notes.
  ------------------------------------------------------------------------*/

#include <stdint.h>
#include <limits.h>
#include <Arduino.h>
#include <Adafruit_DotStar.h>
#ifdef ENABLE_IRREMOTE
# include <IRremote.h>
#endif

/*
 * Idea from: https://learn.adafruit.com/supernova-poi/introduction
 *
 * Teensy-3.2 wiring (valid for the Teensy-4.x as well)
 *
 * Pin 5 (PTD7) IR receiver
 * Pin 11 (PTC6) SPI related (SPI#0 SOUT)
 * Pin 13 (PTC5) SPI related (SPI#0 CLK, ca 12 MHz)
 */
#ifdef TEST
# include <stdio.h>
# define report(fmt,...) printf(fmt, ##__VA_ARGS__)
#else
# define report(fmt,...)
#endif

/**
 * Define the use some pins to measure the required time to do "some things"
 *
 * In order to check, if each required step can be done in time, just signal the
 * consumed time via some GPIOs. I have measured these with my "Saleae" to get
 * an idea, how fast (or slow) the little µC is to send the data to the LEDs
 * and then preprocess the next line. Both steps needs to be done in the time one
 * line of the image is visible (refer definition of the @b line_interval_us
 * variable).
 *
 * Make use of it on demand, by un-commenting the following line.
 */
// #define TIME_MEASURE

#define LED_MEM_PREPARE 1 /* Pin 1 aka PTB17 */
#define LED_SPI_SEND 2 /* Pin 2 aka PDT0 */

#ifdef TIME_MEASURE
# define activate(x) digitalWrite(x, HIGH);
# define deactivate(x) digitalWrite(x, LOW);
# else
# define activate(x)
# define deactivate(x)
#endif

typedef unsigned line_t;

// CONFIGURABLE STUFF ------------------------------------------------------
#include "implementation.h"
/*
 * From the graphics.h we expect here the definition of the macro "NUM_LEDS".
 * It defines the height of the graphics in pixel. Its value is used to read
 * the image data. It must not correspond with the available LED count. For
 * this, refer @b available_led_count and @b pattern_repeat_count
 */
#include "graphics.h" // Graphics data is contained in this header file.

// It's generated using the 'convert.py' Python script.  Various image
// formats are supported, trading off colour fidelity for PROGMEM space.
// Handles 1-, 4- and 8-bit-per-pixel palette-based images, plus 24-bit
// truecolor.  1- and 4-bit palettes can be altered in RAM while running
// to provide additional colors, but be mindful of peak & average current
// draw if you do that!  Power limiting is normally done in convert.py
// (keeps this code relatively small & fast).
// -------------------------------------------------------------------------

/* ---------------------------------------------------------------
 * DotStar related
 */
static Adafruit_DotStar strip = Adafruit_DotStar(available_led_count, DOTSTAR_BGR);

/* ---------------------------------------------------------------
 * Infrared related globals (used on demand)
 */
#ifdef ENABLE_IRREMOTE

/* Get the IR remote control codes */
# include "ir-remote.h"

/**
 * Define the pin to be used for the infrared signal (on demand)
 */
# define RECV_PIN 5

static IRrecv irrecv(RECV_PIN);

/**
 * The time difference from 'now' to the start of the performance, when the
 * performance should pause, e.g. showing the current image "for ever"
 *
 * This difference is used to stop the time from the performance point of view.
 *
 * Regular time flow if its value is '0'
 */
static unsigned long time_diff_to_pause;
#endif

/* --------------------------------------------------------------- */

/**
 * Base time [ms], the performance was started
 * This is the base time all performance progression is calculated on
 *
 * Keep in mind: millis() overruns after 50 days
 */
static unsigned long performance_start_time;

/**
 * The current performance setting. Index into array @b perf
 * A negative value means, the performance is stopped
 */
static ssize_t performance_idx;

/**
 * 0, if the LED memory buffer is empty
 * 1, if the LED memory buffer contains data ready to be sent to the LEDs
 */
static char led_buffer_armed;

/**
 * The current image information we must process on
 */
static const struct image *current_image;

/**
 * The number of the current image line
 * (0…x)
 */
static unsigned current_image_line;

/**
 * The time stamp when the currently displayed image line was sent to the LEDs
 */
static unsigned long current_image_line_time_stamp;

/* --------------------------------------------------------------- */

/**
 * @retval true The performance is running
 * @retval false The performance is stopped
 */
static bool is_performance_running(void)
{
	return performance_idx >= 0;
}

/**
 * Activate a new image
 * @param[in] idx The index into #images for the image to be activated
 */
static void image_activate(size_t idx)
{
	if (idx >= NUM_IMAGES) { /* paranoia */
		/* TODO else: show error? */
		return;
	}

	if ((current_image == NULL) || (current_image_line > images[idx].lines))
		current_image_line = 0;

	current_image = &images[idx];
	led_buffer_armed = 0; /* LED buffer now needs new content */
	report("Image %zu activated\n", idx);
}

/**
 * Clear the LED buffer to switch off all LEDs
 *
 * Mark the buffer as not armed as well. If the loop re-starts it must be filled
 * with valid image data first
 */
static void led_buffer_clear(void)
{
	strip.clear();
	led_buffer_armed = 0;
}

/**
 * (Re-)Initialize global performance state
 *
 * Just ensure the first element of the performance is the current one and
 * the performance base time is *now*.
 * Avoid any anoying flicker by switching all LEDs off first. It should restart
 * with the content of the first line of the new image.
 */
static void performance_init(void)
{
	/* Prepare a reliable state */
	led_buffer_clear();
	strip.show();
	current_image_line = 0;
#ifdef ENABLE_IRREMOTE
	time_diff_to_pause = 0;
#endif
	performance_idx = 0;
	performance_start_time = millis();
	report("Performance init\n");
}

/**
 * Stop the performance statemachine
 */
static void performance_stop(void)
{
	performance_idx = -1; /* finished */
	led_buffer_clear();
	strip.show(); /* switch off all LEDs */
}

/**
 * Expectation is:
 *  - each line in the source image contains "NUM_LEDS / 8" bytes
 *  - eight pixels per byte
 */
static const uint8_t *collum_start_for_palette1(const struct image *image, unsigned collumn)
{
	return &image->pixels[collumn * NUM_LEDS / 8U];
}

/**
 * Expectation is:
 *  - each line in the source image contains "NUM_LEDS / 2" bytes
 *  - two pixels per byte
 */
static const uint8_t *collum_start_for_palette4(const struct image *image, unsigned collumn)
{
	return &image->pixels[collumn * NUM_LEDS / 2U];
}

/**
 * Expectation is:
 *  - each line in the source image contains "NUM_LEDS" bytes
 *  - one pixel per byte
 */
static const uint8_t *collum_start_for_palette8(const struct image *image, unsigned collumn)
{
	return &image->pixels[collumn * NUM_LEDS];
}

/**
 * @param[in] pattern Pixel source, one byte defines eight pixels, MS Bit defines the left pixel
 * @param[in] palette The colour palette to be used for the conversion with two entries
 * @param[in] led_start The LED start number to convert the source to
 */
static void send_palette_1bpp(const uint8_t *pattern, const uint8_t (*palette)[3], unsigned led_start)
{
	size_t idx, color;

	for (idx = 0; idx < NUM_LEDS; idx += 8) {
		color = !!(*pattern & 0x80);
		strip.setPixelColor(led_start++, palette[color][0], palette[color][1], palette[color][2]);
		color = !!(*pattern & 0x40);
		strip.setPixelColor(led_start++, palette[color][0], palette[color][1], palette[color][2]);
		color = !!(*pattern & 0x20);
		strip.setPixelColor(led_start++, palette[color][0], palette[color][1], palette[color][2]);
		color = !!(*pattern & 0x10);
		strip.setPixelColor(led_start++, palette[color][0], palette[color][1], palette[color][2]);
		color = !!(*pattern & 0x08);
		strip.setPixelColor(led_start++, palette[color][0], palette[color][1], palette[color][2]);
		color = !!(*pattern & 0x04);
		strip.setPixelColor(led_start++, palette[color][0], palette[color][1], palette[color][2]);
		color = !!(*pattern & 0x02);
		strip.setPixelColor(led_start++, palette[color][0], palette[color][1], palette[color][2]);
		color = !!(*pattern & 0x01);
		strip.setPixelColor(led_start++, palette[color][0], palette[color][1], palette[color][2]);
		pattern++;
	}
}

/**
 * @param[in] pattern Pixel source, one byte defines two pixels, upper nibble defines the left pixel
 * @param[in] palette The colour palette to be used for the conversion with 16 entries
 * @param[in] led_start The LED start number to convert the source to
 *
 * @attention The @b src buffer is expected to contain enough data for all LEDs.
 *            There isn't any kind of boundary check possible! Enough data means
 *            at least (NUM_LEDS / 2) bytes must be available
 */
static void send_palette_4bpp(const uint8_t *pattern, const uint8_t (*palette)[3], unsigned led_start)
{
	size_t p1, p2, idx;

	for (idx = 0; idx < NUM_LEDS; idx += 2) {
		p1 = p2 = *pattern;	// Data for two pixels...
		p1 >>= 4;		// Shift down 4 bits for first pixel
		p2 &= 0x0F;		// Mask out low 4 bits for second pixel

		strip.setPixelColor(led_start++, palette[p1][0], palette[p1][1], palette[p1][2]);
		strip.setPixelColor(led_start++, palette[p2][0], palette[p2][1], palette[p2][2]);
		pattern++;
	}
}

/**
 * @param[in] pattern Pixel source, one byte defines two pixels, upper nibble defines the left pixel
 * @param[in] palette The colour palette to be used for the conversion, with 256 entries
 * @param[in] led_start The LED start number to convert the source to
 */
static void send_palette_8bpp(const uint8_t *pattern, const uint8_t (*palette)[3], unsigned led_start)
{
	size_t idx;

	for (idx = 0; idx < NUM_LEDS; idx++) {
		strip.setPixelColor(led_start++, palette[*pattern][0], palette[*pattern][1], palette[*pattern][2]);
		pattern++;
	}
}

/**
 * @param[in] pattern Pixel source, one byte defines two pixels, upper nibble defines the left pixel
 * @param[in] led_start The LED start number to convert the source to
 */
static void send_palette_truecolor(const uint8_t *pattern, unsigned led_start)
{
	size_t idx;

	for (idx = 0; idx < NUM_LEDS; idx++) {
		strip.setPixelColor(led_start++, pattern[0], pattern[1], pattern[2]);
		pattern += 3;
	}
}

/**
 * Send one collumn of the input image to the LEDs
 * @param[in] image Base pointer to the image (top, left pixel)
 * @param[in] palette Colour palette information for @b colour_type not TRUECOLOR
 * @param[in] collumn The column to be sent to the LEDs
 * @param[in] color_type The type of the image's colour information
 * @param[in] led_count Full count of LEDs
 *
 * @note The variable @b pattern_repeat_count defines the repeating pattern count of the image pattern
 *       If it is '1' nothing special happes and the image's size is
 *       expected to match the full LED count.
 *       If it is '2' the image's size is expected to match
 *       the half count of LEDs. The same image's pattern is repeated
 *       in the second half of the LEDs.
 */
static void prepare_one_col(const struct image *image, unsigned collumn)
{
	const uint8_t *pattern;
	unsigned led_num;

	report("(");
	activate(LED_MEM_PREPARE);
	switch (image->type) {
	case PALETTE1:
		/* Eight pixels per byte, two colours */
		pattern = collum_start_for_palette1(image, collumn);
		for (led_num = 0; led_num < available_led_count; led_num += NUM_LEDS)
			send_palette_1bpp(pattern, image->palette, led_num);
		break;
	case PALETTE4:
		/* Two pixels per byte, sixteen colours */
		pattern = collum_start_for_palette4(image, collumn);
		for (led_num = 0; led_num < available_led_count; led_num += NUM_LEDS)
			send_palette_4bpp(pattern, image->palette, led_num);
		break;
	case PALETTE8:
		/* One pixel per byte, 256 colours */
		pattern = collum_start_for_palette8(image, collumn);
		for (led_num = 0; led_num < available_led_count; led_num += NUM_LEDS)
			send_palette_8bpp(pattern, image->palette, led_num);
		break;
	case TRUECOLOR:
		/* One pixel per three byte, 16 million colours */
		pattern = &image->pixels[collumn * NUM_LEDS * 3U];
		for (led_num = 0; led_num < available_led_count; led_num += NUM_LEDS)
			send_palette_truecolor(pattern, led_num);
		break;
	}
	deactivate(LED_MEM_PREPARE);
	report(")");
}

/**
 * Show a time stamp mismatch error
 * @param[in] index At what index the time stamp error was found
 *
 * This function never returns
 */
static void error_time_stamp_show(size_t index)
{
	size_t u;

	if (index >= available_led_count)
		index = available_led_count - 1; /* limit it to the available LEDs */

	do {
		/* lets blink */
		strip.clear();
		strip.show(); /* switch off the LEDs */
		delay(100);
		for (u = 0; u <= index; u++)
			strip.setPixelColor(u, 255, 0, 0); /* full red */
		strip.show(); /* show the error pattern */
		delay(500);
	} while (1);
}

/**
 * Show an error in the performance array
 *
 * This function never returns
 */
static void error_performance_length_show(void)
{
	do {
		strip.clear();
		strip.show(); /* switch off the LEDs first */
		delay(100);
		strip.setPixelColor(0, 255, 255, 0); /* full yellow */
		strip.setPixelColor(1, 255, 255, 0); /* full yellow */
		strip.setPixelColor(2, 255, 255, 0); /* full yellow */
		strip.setPixelColor(3, 255, 255, 0); /* full yellow */
		strip.setPixelColor(5, 255, 255, 0); /* full yellow */
		strip.show(); /* show the error pattern */
		delay(500);
	} while (1);
}

/**
 * Detect if it is time to refresh the LEDs
 * @retval false No, keep the current state
 * @retval true Time to refresh
 *
 * @pre #lastLineTime needs to be set when the LEDs were refreshed the last time
 */
static bool determine_point_of_activation(void)
{
	if ((micros() - current_image_line_time_stamp) < line_interval_us)
		return false; /* Interval not reached, yet */

	return true;
}

/**
 * Calculate a difference in a save manner (e.g. honor rollovers)
 * @return minuend - subtrahend
 *
 * Used to deal with the time in milliseconds. The returned time in milliseconds
 * can roll over and then the @b performance_start_time can be larger than the
 * time in milliseconds. Deal with it.
 */
static unsigned long save_substraction(unsigned minuend, unsigned subtrahend)
{
	unsigned long ret;

	if (subtrahend < minuend)
		return minuend - subtrahend;

	/* A roll over has happend */
	ret = ULONG_MAX;
	ret -= subtrahend;
	ret++;
	ret += minuend;

	return ret;
}

/**
 * Calculate the time[ms] elapsed since the start of this performance
 * @return Elapsed time since the start of this performance (in [ms])
 *
 * If the performance should stall (e.g. PAUSE function) the performance start
 * time gets updated to keep the returned time constant. Thus, the current image
 * gets displayed as long as the user wants it.
 *
 * @pre #performance_start_time needs to be set with the current time when the performance starts
 */
static unsigned long performance_elapsed_time_get(void)
{
#ifdef ENABLE_IRREMOTE
	if (time_diff_to_pause != 0)
		performance_start_time = save_substraction(millis(), time_diff_to_pause);
#endif

	return save_substraction(millis(), performance_start_time);
}

/**
 * Move to the next performance entry if its time stamp is reached
 */
static void performance_progression(void)
{
	unsigned long now;

	if (!is_performance_running())
		return; /* finished, no more progression */

	now = performance_elapsed_time_get();
	if (now < perf[performance_idx].act_time)
		return; /* nothing to be done here, yet */

	if (perf[performance_idx].image_no >= 0) {
		image_activate(perf[performance_idx].image_no);
		/* check if the next time stamp is in the future */
		if (performance_idx < (performance_length - 1)) {
			if (perf[performance_idx + 1].act_time > perf[performance_idx].act_time)
				performance_idx++; /* move to the next performance entry */
			else
				error_time_stamp_show(performance_idx + 1);
		} else
			error_performance_length_show();
		return;
	}

	if (perf[performance_idx].image_no == PERFORMANCE_STOP) {
		performance_stop();
		report("Performance terminated\n");
		return;
	}

	/* repeat */
	performance_init();
	report("Performance repeat\n");
}

/**
 * Move to the next scanline in the image. If all done, repeat
 */
static void image_progression(void)
{
	current_image_line++;
	if (current_image_line >= current_image->lines) {
		current_image_line = 0; /* bottom line reached, re-start at the top line */
		report("Image repeat\n");
	}
}

#ifdef ENABLE_IRREMOTE
/**
 * Handle the START / PAUSE IR remote control button
 *
 * If this button is pressed:
 *  - if the performance is stopped -> (re-)start it
 *  - if the performance is running -> stall it
 *  - if the performance is paused -> continue it
 *
 * @note This button and its function is documented in the #ENABLE_IRREMOTE
 */
static void ir_remote_start_pause_handler(void)
{
	if (!is_performance_running()) {
		/* if really stopped, just *restart* it */
		performance_init();
		return;
	}

	/* START <---> PAUSE on each press */
	if (time_diff_to_pause == 0)
		time_diff_to_pause = save_substraction(millis(), performance_start_time); /* stall at this point of time */
	else
		time_diff_to_pause = 0; /* continue */
}
#endif

/**
 * Deal with IR based remote control
 */
static void ir_remote_handle(void)
{
#ifdef ENABLE_IRREMOTE
	static unsigned long last_code; /* to deal with repeating codes */
	unsigned long current_code;
	decode_results results;

	if (!irrecv.decode(&results))
		return; /* Nothing received yet. We are done here */

	irrecv.resume(); /* Restart the statemachine for the next reception */

	/* button repeat handling */
	if (results.bits != 32) { /* NEC protocol returns 32 bits on a new button */
		if (last_code == 0)
			return; /* ignore repeated buttons */
		current_code = last_code;
	} else
		current_code = results.value;

	switch (current_code) {
	case MRC_BTN_VOL_UP:
	case SBC_BTN_VOL_UP:
		/* decrease the time for one line */
		if (line_interval_us > line_interval_min_us)
			line_interval_us -= 10;
		last_code = MRC_BTN_VOL_UP; /* support repeat */
		break;
	case MRC_BTN_PLAYPAUSE:
	case SBC_BTN_PLAYPAUSE:
		ir_remote_start_pause_handler();
		last_code = 0; /* don't repeat this button */
		break;
	case MRC_BTN_VOL_DOWN:
	case SBC_BTN_VOL_DOWN:
		/* increase the time for one line */
		if (line_interval_us < line_interval_max_us)
			line_interval_us += 10;
		last_code = MRC_BTN_VOL_DOWN; /* support repeat */
		break;
	case MRC_BTN_SETUP:
	case SBC_BTN_SETUP:
		break; /* ignored */
	case MRC_BTN_UP:
	case SBC_BTN_UP:
		if (led_brightness < 255) {
			/* '255' means full brightness */
			led_brightness++;
			strip.setBrightness(led_brightness);
		}
		last_code = MRC_BTN_UP; /* support repeat */
		break;
	case MRC_BTN_STOPMODE:
	case SBC_BTN_STOPMODE:
		performance_stop();
		last_code = 0; /* don't repeat this button */
		break;
	case MRC_BTN_LEFT:
	case SBC_BTN_LEFT:
		break; /* ignored */
	case MRC_BTN_ENTERSAVE:
	case SBC_BTN_ENTERSAVE:
		break; /* ignored */
	case MRC_BTN_RIGHT:
	case SBC_BTN_RIGHT:
		break; /* ignored */
	case MRC_BTN_ZERO10PLUS:
	case SBC_BTN_ZERO10PLUS:
		break; /* ignored */
	case MRC_BTN_DOWN:
	case SBC_BTN_DOWN:
		if (led_brightness != 0) {
			/* '0' means more or less off */
			led_brightness--;
			strip.setBrightness(led_brightness);
		}
		last_code = MRC_BTN_DOWN; /* support repeat */
		break;
	case MRC_BTN_REPEAT:
	case SBC_BTN_REPEAT:
		performance_init();
		last_code = 0; /* don't repeat this button */
		break;
	case MRC_BTN_ONE:
	case SBC_BTN_ONE:
		break; /* ignored */
	case MRC_BTN_TWO:
	case SBC_BTN_TWO:
		break; /* ignored */
	case MRC_BTN_THREE:
	case SBC_BTN_THREE:
		break; /* ignored */
	case MRC_BTN_FOUR:
	case SBC_BTN_FOUR:
		break; /* ignored */
	case MRC_BTN_FIVE:
	case SBC_BTN_FIVE:
		break; /* ignored */
	case MRC_BTN_SIX:
	case SBC_BTN_SIX:
		break; /* ignored */
	case MRC_BTN_SEVEN:
	case SBC_BTN_SEVEN:
		break; /* ignored */
	case MRC_BTN_EIGHT:
	case SBC_BTN_EIGHT:
		break; /* ignored */
	case MRC_BTN_NINE:
	case SBC_BTN_NINE:
		break; /* ignored */
	}
#endif
}

static void setup_ios(void)
{
#ifdef TIME_MEASURE
	pinMode(LED_MEM_PREPARE, OUTPUT);
	pinMode(LED_SPI_SEND, OUTPUT);
#endif
}

/**
 * Setup the IR reception (on demand only)
 */
static void setup_irrec(void)
{
#ifdef ENABLE_IRREMOTE
	irrecv.enableIRIn();
#endif
}

/**
 * Start the performance automatically (if enabled)
 *
 * For the IR remote control use case, the performance should not start
 * automatically. Exception is if the macro #BEGIN_PERF_ON_START is defined
 * and forces it.
 */
static void enable_performance(void)
{
#if defined(ENABLE_IRREMOTE) && !defined(BEGIN_PERF_ON_START)
	performance_stop(); /* don't start automatically, wait for the START IR code */
#else
	performance_init(); /* prepare starting the performance */
#endif
}

/* --------------- Connectors into the Arduino world ------------------ */

/**
 * Called once at startup
 */
void setup(void)
{
	setup_ios();

	strip.begin(); // Allocate DotStar buffer, init SPI
	strip.setBrightness(led_brightness); // Set the user's initial brightness
	enable_performance();
	setup_irrec(); /* start the infrared receiver (on demand only) */
}

/**
 * Called ever and ever again
 */
void loop(void)
{
	if (!is_performance_running()) {
		ir_remote_handle(); /* keep IR enabled (for a restart for example) */
		return; /* finished, no more progression */
	}

	/* if the interval is reached, send the prepared data to the LEDs */
	if (led_buffer_armed && determine_point_of_activation()) {
		current_image_line_time_stamp = micros(); /* keep point of time of activation */
		activate(LED_SPI_SEND);
		strip.show(); /* show the buffer content */
		deactivate(LED_SPI_SEND);
		report(">");
		led_buffer_armed = 0; /* LED buffer sent */
		image_progression();
	}

	/* Switch the displayed image on demand */
	performance_progression();

	ir_remote_handle();

	if (led_buffer_armed != 0) {
		report(".");
		return; /* done for now */
	}

	/*
	 * Prepare the next image line to be sent to the LEDs. This setup
	 * happens in memory for now, e.g. offline.
	 */
	prepare_one_col(current_image, current_image_line);
	led_buffer_armed = 1;
}

// kate: tab-indents on; tab-width 4; indent-width 4; replace-tabs off; eol unix; hl = c++; keep-extra-spaces false; remove-trailing-spaces all; show-tabs on;
