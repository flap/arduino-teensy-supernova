/* SPDX-License-Identifier: CC0-1.0 */
#ifndef IR_REMOTE_H
# define IR_REMOTE_H

/**
 * Definitions for some IR remote controlers
 *
 * Fun fact:
 * - the IR remote control from Adafruit is: Mini Remote Control (PRODUCT ID: 389)
 * - the IR remote control from JOY-IT is: SBC-IRC01
 *
 * They look the same, but they send different codes
 *
 * Encoding scheme: NEC
 * Carrier frequency: 38 kHz, 940nm IR LED
 */

/* The Adafruit Mini Remote Control */
/* First row */
# define MRC_BTN_VOL_UP		0xFD40BF
# define MRC_BTN_PLAYPAUSE	0xFD807F
# define MRC_BTN_VOL_DOWN	0xFD00FF
/* Second row */
# define MRC_BTN_SETUP		0xFD20DF
# define MRC_BTN_UP			0xFDA05F
# define MRC_BTN_STOPMODE	0xFD609F
/* Third row */
# define MRC_BTN_LEFT		0xFD10EF
# define MRC_BTN_ENTERSAVE	0xFD906F
# define MRC_BTN_RIGHT		0xFD50AF
/* Fourth row */
# define MRC_BTN_ZERO10PLUS	0xFD30CF
# define MRC_BTN_DOWN		0xFDB04F
# define MRC_BTN_REPEAT		0xFD708F
/* Fifth row */
# define MRC_BTN_ONE		0xFD08F7
# define MRC_BTN_TWO		0xFD8877
# define MRC_BTN_THREE		0xFD48B7
/* Sixth row */
# define MRC_BTN_FOUR		0xFD28D7
# define MRC_BTN_FIVE		0xFDA857
# define MRC_BTN_SIX		0xFD6897
/* Eighth row */
# define MRC_BTN_SEVEN		0xFD18E7
# define MRC_BTN_EIGHT		0xFD9867
# define MRC_BTN_NINE		0xFD58A7

/* The JOY-IT SBC-IRC01 */
/* First row */
# define SBC_BTN_VOL_UP		0xFFE21D
# define SBC_BTN_PLAYPAUSE	0xFF629D
# define SBC_BTN_VOL_DOWN	0xFFA25D
/* Second row */
# define SBC_BTN_SETUP		0xFF22DD
# define SBC_BTN_UP			0xFF02FD
# define SBC_BTN_STOPMODE	0xFFC23D
/* Third row */
# define SBC_BTN_LEFT		0xFFE01F
# define SBC_BTN_ENTERSAVE	0xFFA857
# define SBC_BTN_RIGHT		0xFF906F
/* Fourth row */
# define SBC_BTN_ZERO10PLUS	0xFF6897
# define SBC_BTN_DOWN		0xFF9867
# define SBC_BTN_REPEAT		0xFFB04F
/* Fifth row */
# define SBC_BTN_ONE		0xFF30CF
# define SBC_BTN_TWO		0xFF18E7
# define SBC_BTN_THREE		0xFF7A85
/* Sixth row */
# define SBC_BTN_FOUR		0xFF10EF
# define SBC_BTN_FIVE		0xFF38C7
# define SBC_BTN_SIX		0xFF5AA5
/* Eighth row */
# define SBC_BTN_SEVEN		0xFF42BD
# define SBC_BTN_EIGHT		0xFF4AB5
# define SBC_BTN_NINE		0xFF52AD

#endif

// kate: tab-indents on; tab-width 4; indent-width 4; replace-tabs off; eol unix; hl = c++; keep-extra-spaces false; remove-trailing-spaces all; show-tabs on;
