/* SPDX-License-Identifier: CC0-1.0 */

#ifndef IMPLEMENTATION_H
# define IMPLEMENTATION_H

#include <stddef.h>

/*
 * Define the structure of a performance
 */
struct performance {
	unsigned long act_time; /**< Activation time in [ms] (zero based) */
	ssize_t image_no; /**< Image to show, index into the images[] array */
};

#define AT(seconds) .act_time = (unsigned long)((seconds)*1000.0)

/**
 * Setup an image command
 */
#define IMAGE_NO(x) .image_no = x

/**
 * Value to detect a repeat command
 */
#define PERFORMANCE_REPEAT -1

/**
 * To be used instead of an Image to signal the performance should be repeated
 */
#define PERF_REPEAT IMAGE_NO(PERFORMANCE_REPEAT)

/**
 * Value to detect a stop command
 */
#define PERFORMANCE_STOP -2

/**
 * To be used instead of an Image to signal the performance should be stopped.
 * A power cycle is required in order to re-start the performance again
 */
#define PERF_STOP IMAGE_NO(PERFORMANCE_STOP)

#endif

// kate: tab-indents on; tab-width 4; indent-width 4; replace-tabs off; eol unix; hl = c++; keep-extra-spaces false; remove-trailing-spaces all; show-tabs on;
